package captain.org.product.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import captain.org.cache.CustomerCache;
import captain.org.core.common.CloudResult;
import captain.org.core.controller.BaseController;
import captain.org.core.enums.ResultEnum;
import captain.org.product.exception.ProductException;
import captain.org.product.exception.ProductMessageEnum;

@RestController
public class ProductController extends BaseController {

    @Autowired
    CustomerCache customerCache;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${server.port}")
    private String serverPort;

    @Value("${custom.test-value:none}")
    private String testValue;

    @PostMapping("/app-name/{from}")
    public CloudResult echoAppName(@PathVariable String from, @RequestBody  JSONObject jsonObject) {
        if ("F0001".equals(from)) {
            throw new ProductException(ProductMessageEnum.F0001.getCode());
        }
        if ("F0002".equals(from)) {
            throw new ProductException(
                ProductMessageEnum.F0002.getCode(), new Object[] { "订单已存在" });
        }
        if ("other".equals(from)) {
            throw new ProductException(ResultEnum.OTHER_ERROR.getCode());
        }
        return getSuccessResult(serverPort + "@" + testValue);
    }

    @GetMapping("/get/test-value")
    public String getTestValue() {
        customerCache.get("123");
        return testValue + "@" + serverPort;
    }
}
