package captain.org.product.handler;

import org.springframework.web.bind.annotation.RestControllerAdvice;

import captain.org.core.exception.BaseExceptionHandler;

@RestControllerAdvice
public class CustomExceptionHandler extends BaseExceptionHandler {
}
