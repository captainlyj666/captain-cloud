package captain.org.product.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

import captain.org.core.util.ApplicationContextUtil;

@Configuration
public class OtherStartConfiguration {
    @Autowired
    MessageSource messageSource;

    @Bean
    public ApplicationContextUtil applicationContextUtil() {
        return new ApplicationContextUtil();
    }

}
