package captain.org.product.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductMessageEnum {
    F0001("F0001", "001"),
    F0002("F0002", "002");
    private String code;
    private String desc;
}
