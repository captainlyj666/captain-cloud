package captain.org.product.exception;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import captain.org.core.exception.BaseException;
import captain.org.core.util.ApplicationContextUtil;

public class ProductException extends BaseException {
    private static final  long serialVersionUID = 1L;
    private static final MessageSource MESSAGE_SOURCE;

    static {
        MESSAGE_SOURCE = ApplicationContextUtil.getApplicationContext().getBean(MessageSource.class);
    }

    public ProductException(String code) {
        super(code, MESSAGE_SOURCE.getMessage(code, null, LocaleContextHolder.getLocale()));
        revertCode(code, null);
    }

    public ProductException(String code, Object[] objects) {
        super(code, MESSAGE_SOURCE.getMessage(code, objects, LocaleContextHolder.getLocale()));
        revertCode(code, null);
    }

    @Override
    protected void revertCode(String code, Throwable cause) {
        setErrorCode(getCode());
        setFriendlyMessage(getMessage());
    }
}
