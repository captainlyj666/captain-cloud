package captain.org.auth.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Auth2ResponseExceptionTranslator implements WebResponseExceptionTranslator {

    //自定义异常返回
    @Override
    public ResponseEntity translate(Exception e) throws Exception {
        log.error("授权服务器 异常统一处理:异常:{}", e.getMessage(), e);
        return null;
    }
}
