/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package captain.org.auth.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthUserDetailsService extends UserDetailsService {

    /**
     * 根据手机号查询
     * @param mobile
     * @return
     */
    UserDetails loadUserByMoblie(String mobile);


    /**
     * QQ 登录
     * @param qqOpenid
     * @return
     */
    UserDetails loginByQQ(String qqOpenid);


    /**
     * 微信登录
     * @param wxOpenid
     * @return
     */
    UserDetails loginByWX(String wxOpenid);
}
