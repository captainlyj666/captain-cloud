/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package captain.org.auth.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import captain.org.auth.service.AuthUserDetailsService;
import captain.org.core.security.SecurityUser;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AuthUserDetailsServiceImpl implements AuthUserDetailsService {

//    @Autowired
//    private SysUserService sysUserService;


    @Override
    public UserDetails loadUserByMoblie(String mobile) {
        return new SecurityUser("captainUserName", "$2a$10$PwOSS.IUDBu5tXfz8v3reujZ.225A8LkVQYHQZ9Ma0Vh/3BO.Vfk.", add());
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return new SecurityUser("captainUserName", "$2a$10$PwOSS.IUDBu5tXfz8v3reujZ.225A8LkVQYHQZ9Ma0Vh/3BO.Vfk.", add());
    }

    @Override
    public UserDetails loginByQQ(String qqOpenid) {
        return new SecurityUser("captainUserName", "$2a$10$PwOSS.IUDBu5tXfz8v3reujZ.225A8LkVQYHQZ9Ma0Vh/3BO.Vfk.", add());
    }

    @Override
    public UserDetails loginByWX(String wxOpenid) {
        return new SecurityUser("captainUserName", "$2a$10$PwOSS.IUDBu5tXfz8v3reujZ.225A8LkVQYHQZ9Ma0Vh/3BO.Vfk.", add());
    }

    private List<? extends GrantedAuthority> add() {
        List<SimpleGrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority("admin"));
        return list;
    }
}
