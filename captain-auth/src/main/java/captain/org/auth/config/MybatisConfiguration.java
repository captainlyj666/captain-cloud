package captain.org.auth.config;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = MybatisConfiguration.BASE_PACKAGES, sqlSessionFactoryRef = MybatisConfiguration.SQL_SESSION_FACTORY_REF)
public class MybatisConfiguration {
    public static final String DATASOURCE_PREFIX = "spring.datasource.auth";
    public static final String DATASOURCE_REF = "authSource";
    public static final String BASE_PACKAGES = "captain.org.auth.dao";
    public static final String SQL_SESSION_FACTORY_REF = "authSqlSessionFactory";
    public static final String TRANSACTION_MANAGER_REF = "authTransactionManager";
    public static final String MAPPER_LOCATIONS = "classpath*:mappers/auth/*.xml";

    @Bean(DATASOURCE_REF)
    @ConfigurationProperties(prefix = DATASOURCE_PREFIX)
    @Primary
    public HikariDataSource dataSource() {
        final HikariDataSource dataSource = DataSourceBuilder.create().type(HikariDataSource.class).build();
        return dataSource;
    }

    @Bean(TRANSACTION_MANAGER_REF)
    @Primary
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean(SQL_SESSION_FACTORY_REF)
    @Primary
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        final MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource());
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactoryBean.setMapperLocations(resolver.getResources(MAPPER_LOCATIONS));
        final com.baomidou.mybatisplus.core.MybatisConfiguration configuration = new com.baomidou.mybatisplus.core.MybatisConfiguration();
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setCacheEnabled(true);
        sqlSessionFactoryBean.setConfiguration(configuration);
        sqlSessionFactoryBean.setPlugins(new Interceptor[]{
                paginationInterceptor(),
        });
        return sqlSessionFactoryBean.getObject();
    }

    /**
     * 分页.
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
