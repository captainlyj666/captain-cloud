# **nacos springcloud Oauth2** 

简介
		配置中心/注册中心 nacos 
		认证 Oauth2
		限流 alibaba sentinel 
		在线文档 kinfe4j

db 附件

| 服务名          | 端口号 | 描述            |
| --------------- | ------ | --------------- |
| captain-gateway | 9999   | 网关            |
| captain-auth    | 8888   | 认证            |
| captain-api-doc | 7777   | 网关api在线文档 |
| captain-core    |        | 公用            |
|                 |        |                 |

OAuth2

client_credentials  不支持刷新token method.POST
http://localhost:8888/oauth/token?grant_type=client_credentials&client_id=captain&client_secret=captain
{
    "access_token": "007248db-cf7d-42c6-b8a6-531db1f36a07",
    "token_type": "bearer",
    "expires_in": 2822,
    "scope": "scope1"
}


刷新token POST
http://localhost:8888/oauth/token?grant_type=refresh_token&refresh_token=089b6e45-89ae-4589-8e72-6a2dc11b87ee

检查token  POST
http://localhost:8888/oauth/check_token?token=007248db-cf7d-42c6-b8a6-531db1f36a07



password
http://localhost:8888/oauth/token?grant_type=password&client_id=captain&client_secret=captain&username=captain&password=captain




http://localhost:8888/oauth/authorize?

获取TOKEN
http://localhost:8888/oauth/authorize?response_type=code&client_id=client1&redirect_uri=https://qds.jusding.top

172.26.96.97:8888/oauth/authorize?response_type=code&client_id=client1&redirect_uri=https://www.baidu.com



2021-11-10 

add messageSource  captain-product-examlpe

2021-11-12

add version 灰度

nacos metadata 元素 添加verison  

网关可以根据 version   header里面添加 appVersion 选择那个服务
