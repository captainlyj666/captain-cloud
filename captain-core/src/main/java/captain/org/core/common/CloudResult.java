package captain.org.core.common;

import java.io.Serializable;

import captain.org.core.enums.ResultEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class CloudResult<T> implements Serializable {

    private static final long serialVersionUID = -5816713866887895850L;

    private String code = ResultEnum.SUCCESS.getCode();

    /**
     * 错误信息
     */
    private String msg = null;

    /**
     * 返回结果实体
     */
    private T data = null;

    public CloudResult() {
    }

    public CloudResult(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

}
