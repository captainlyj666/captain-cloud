package captain.org.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import captain.org.core.common.CloudResult;
import captain.org.core.enums.ResultEnum;

public class BaseController {

    @Autowired
    protected MessageSource messageSource;

    /**
     * 转换成功结果.
     *
     * @param o 结果
     * @return 转换结果
     */
    protected CloudResult getSuccessResult(Object o) {
        return new CloudResult(ResultEnum.SUCCESS.getCode(),
                messageSource.getMessage(ResultEnum.SUCCESS.getCode(),
                        null, LocaleContextHolder.getLocale()), o);
    }
}
