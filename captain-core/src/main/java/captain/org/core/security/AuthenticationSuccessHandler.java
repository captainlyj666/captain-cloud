package captain.org.core.security;

import java.io.IOException;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private static Logger logger = LogManager.getLogger(AuthenticationSuccessHandler.class.getName());

    @Value("${authorization.client-id}")
    private String clientId;

    @Value("${authorization.client-secret}")
    private String clientSecret;

    @Resource
    private ClientDetailsService clientDetailsService;

    @Resource
    private AuthorizationServerTokenServices systemTokenServices;

    @Resource
    private PasswordEncoder securityPasswordEncoder;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);

        if (clientDetails == null) {
            throw new UnapprovedClientAuthenticationException("clientId 对应的客户端不存在:" + clientId);
        } else if (!securityPasswordEncoder.matches(clientSecret, clientDetails.getClientSecret())) {
            throw new UnapprovedClientAuthenticationException("clientSecret 错误:" + clientId);
        }
        TokenRequest tokenRequest = new TokenRequest(new HashMap<>(1), clientId, clientDetails.getScope(), "password");

        OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);

        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);

        OAuth2AccessToken token = systemTokenServices.createAccessToken(oAuth2Authentication);

        // 跳转到首页
        response.sendRedirect("/page/test/test?token=" + token.getValue());

/*        String username = request.getParameter("username");
        String password = request.getParameter("password");
//        username = getURLEncoderString(username);

        Wrapper result = tokenController.loadToken(username,password);
        response.setStatus(HttpStatus.OK.value());
        response.setHeader("Content-Type", "application/json;charset=UTF-8");
        if (result == null) {
            response.getWriter().println(Wrapper.error().toJsonString());
            return;
        }
        if (result.getCode() != ResponseConstant.SUCCESS_CODE) {
            response.getWriter().println(Wrapper.error().toJsonString());
            return;
        }

        response.sendRedirect("/page/test/test?token=" + result.getResult());*/
    }
}
