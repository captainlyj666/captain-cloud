package captain.org.core.exception;

import lombok.Data;

@Data
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 8458544317507845657L;
    private String errorCode;
    private String code;
    private String friendlyMessage = "";
    private Object[] messageArgs;
    private String defaultFriendlyMessage;

    public BaseException(String code, Throwable cause) {
        super(cause);
        this.revertCode(code, cause);
        this.code = code;
    }

    public BaseException(String code, String logMsg) {
        super(logMsg);
        this.revertCode(code, (Throwable)null);
        this.code = code;
    }

    protected void revertCode(String code, Throwable cause) {
        if (null != code) {
            this.errorCode = code;
        }
    }
}
