package captain.org.core.exception;

import lombok.Data;

@Data
public class AopCheckException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private Object[] params;

    public AopCheckException(Object[] params) {
        super("aop check failed.");
        this.params = params;
    }
}
