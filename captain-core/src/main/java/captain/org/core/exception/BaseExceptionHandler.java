package captain.org.core.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;

import captain.org.core.common.CloudResult;
import captain.org.core.enums.ResultEnum;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BaseExceptionHandler {
    @Autowired
    protected MessageSource messageSource;

    public BaseExceptionHandler() {
    }

    @ExceptionHandler({AopCheckException.class})
    @Order(100)
    public CloudResult getAopCheckErrorResult(AopCheckException e) {
        log.error("AopCheckException :", e);
        return new CloudResult(ResultEnum.CHECK_PARAM.getCode(), this.messageSource.getMessage(ResultEnum.CHECK_PARAM.getCode(), e.getParams(), LocaleContextHolder.getLocale()), (Object)null);
    }

    @ExceptionHandler({BaseException.class})
    @Order(1000)
    public CloudResult getCheckErrorResult(BaseException e) {
        log.error("BaseException :", e);
        return new CloudResult(e.getErrorCode(), e.getFriendlyMessage(), (Object)null);
    }

    @ExceptionHandler({Exception.class})
    public CloudResult getErrorResult(Exception e) {
        log.error("Exception :", e);
        return new CloudResult(ResultEnum.OTHER_ERROR.getCode(), this.messageSource.getMessage(ResultEnum.OTHER_ERROR.getCode(), (Object[])null, LocaleContextHolder.getLocale()), (Object)null);
    }
}
