package captain.org.cache;

public interface BaseCache {

    Object get(String key) ;

    void put(String key, Object value);

    void evict(String key);

    void clear();

    default boolean exists(String key) {
        return get(key) != null;
    }
}
