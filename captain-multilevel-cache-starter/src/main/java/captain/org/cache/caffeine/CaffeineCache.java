package captain.org.cache.caffeine;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import com.github.benmanes.caffeine.cache.Cache;

import captain.org.cache.BaseCache;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class CaffeineCache implements BaseCache {
    private Cache<String, Object> cache;

    @Override
    public Object get(String key) {
        log.info("caffeine[get]");
        if(!StringUtils.isEmpty(key)) {
           return cache.getIfPresent(key);
        }
        return null;
    }

    @Override
    public void put(String key, Object value) {
        if(!StringUtils.isEmpty(key)) {
            cache.put(key, value);
        }
    }

    @Override
    public void evict(String  key) {
        if(!StringUtils.isEmpty(key)) {
            cache.invalidateAll(Arrays.asList(key));
        }
    }

    @Override
    public void clear() {
        cache.invalidateAll();
    }

}
