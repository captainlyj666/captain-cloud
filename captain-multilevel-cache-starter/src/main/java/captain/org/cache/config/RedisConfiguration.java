package captain.org.cache.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import captain.org.cache.caffeine.CaffeineCache;
import captain.org.cache.redis.RedisCache;

@Configuration
public class RedisConfiguration {

    private final static String CACHE_NAME = "redisCacheManager";

    @Bean("redissonCacheClient")
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer()
            .setPassword("sOd3bZBFgdVyGRWC")
            .setDatabase(11)
            .setAddress("redis://10.42.0.23:6379");
        return Redisson.create(config);
    }

//    @Bean(CACHE_NAME)
//    public CacheManager cacheManager(@Qualifier("redissonCacheClient") RedissonClient redissonClient) {
//        return new RedissonSpringCacheManager(redissonClient);
//    }

    @Bean("RedisCache")
    public RedisCache redisCache() {
        return RedisCache.builder().redissonClient(redissonClient()).build();
    }
}
