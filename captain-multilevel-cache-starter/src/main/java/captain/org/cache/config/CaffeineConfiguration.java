package captain.org.cache.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Caffeine;

import captain.org.cache.caffeine.CaffeineCache;

@Configuration
public class CaffeineConfiguration {
    private final static String CACHE_NAME = "caffeineCacheManager";

    @Bean(CACHE_NAME)
    public CacheManager cacheManager() {
        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
        caffeineCacheManager.setAllowNullValues(false);
        caffeineCacheManager.setCaffeine(caffeineBuild());
        return caffeineCacheManager;
    }

    @Bean("CaffeineCache")
    public CaffeineCache caffeineCache() {
        return CaffeineCache.builder().cache(caffeineBuild().build()).build();
    }

    @Bean
    public Caffeine caffeineBuild() {
        return Caffeine.newBuilder()
            .expireAfterAccess(60, TimeUnit.SECONDS)
            .initialCapacity(100000);
    }
}
