package captain.org.cache.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import captain.org.cache.BaseCache;
import captain.org.cache.CustomerCache;

@Configuration
@Import({CaffeineConfiguration.class,RedisConfiguration.class})
public class CacheAutoConfiguration implements ApplicationContextAware {

    private ApplicationContext ctx;

    @Bean
    public CustomerCache customerCache() {
        Map<String, BaseCache> map =ctx.getBeansOfType(BaseCache.class);
       ConcurrentHashMap cacheMaps = new ConcurrentHashMap();
       for(Map.Entry entry: map.entrySet()) {
           cacheMaps.put(entry.getKey(), entry.getValue());
       }
        return new CustomerCache(cacheMaps);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if(ctx == null) {
            ctx = applicationContext;
        }
    }
}
