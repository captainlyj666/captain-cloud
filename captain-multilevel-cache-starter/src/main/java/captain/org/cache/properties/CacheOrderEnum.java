package captain.org.cache.properties;

import lombok.Getter;

@Getter
public enum CacheOrderEnum {
    CAFFEINE_CACHE(100,  "caffeineCache"),
    REDIS_CACHE(200,"redisCache");

    private Integer order;

    private String cachePrefix;

    CacheOrderEnum(Integer order, String cachePrefix) {
        this.order = order;
        this.cachePrefix = cachePrefix;
    }
}
