package captain.org.cache.redis;

import java.util.concurrent.TimeUnit;

import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import captain.org.cache.BaseCache;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class RedisCache implements BaseCache {

    private RedissonClient redissonClient;

    @Override
    public Object get(String key) {
        log.info("redis[get]");
        RBucket<Object> bucket = redissonClient.getBucket(key);
        return bucket;
    }

    @Override
    public void put(String key, Object value) {
        RBucket<Object> bucket = redissonClient.getBucket(key);
        bucket.set(value);
        bucket.expire(60, TimeUnit.SECONDS);
    }

    @Override
    public void evict(String key) {
        RBucket<Object> bucket = redissonClient.getBucket(key);
        bucket.delete();
    }

    @Override
    public void clear() {

    }
}
