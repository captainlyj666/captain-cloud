package captain.org.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class CustomerCache implements  BaseCache {

    private ConcurrentMap<String, BaseCache> cacheMap;

    public CustomerCache(ConcurrentMap<String, BaseCache> cacheMap) {
        this.cacheMap = cacheMap;
    }

    @Override
    public Object get(String key) {
        for(Map.Entry entry: cacheMap.entrySet()) {
            BaseCache cache = (BaseCache)entry.getValue();
            if(cache.exists(key)) {
                return cache.get(key);
            };
        }
        return null;
    }

    @Override
    public void put(String key, Object value) {

    }

    @Override
    public void evict(String key) {

    }

    @Override
    public void clear() {

    }

}
