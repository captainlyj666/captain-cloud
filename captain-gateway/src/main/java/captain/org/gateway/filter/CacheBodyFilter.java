package captain.org.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import captain.org.gateway.decorator.CachedRequestDecorator;
import captain.org.gateway.decorator.CachedResponseDecorator;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class CacheBodyFilter implements GlobalFilter, Ordered {

    @Override
    public int getOrder() {
        return FilterOrder.CACHEBODY.getOrder();
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 请求装饰
        final CachedRequestDecorator requestDecorator = new CachedRequestDecorator(exchange);
        // 响应装饰
        final CachedResponseDecorator responseDecorator = new CachedResponseDecorator(exchange);
        return requestDecorator.cacheBody().then(chain.filter(exchange.mutate().request(requestDecorator).response(responseDecorator).build()));
    }
}
