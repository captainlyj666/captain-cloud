package captain.org.gateway.filter;

import org.springframework.cloud.gateway.filter.NettyWriteResponseFilter;
import org.springframework.core.Ordered;

import lombok.Getter;

/**
 * 过滤器顺序定义.
 *
 * @author Leeyc
 */
@Getter
public enum FilterOrder {

    Auth(0),

    LoadBalance(10150),

    LOG(-10000),

    CACHEBODY(-20000);

    private final int order;

    FilterOrder(int order) {
        this.order = order;
    }

}
