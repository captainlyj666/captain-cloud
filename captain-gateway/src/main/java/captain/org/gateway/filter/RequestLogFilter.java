package captain.org.gateway.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.alibaba.fastjson.JSONObject;

import captain.org.gateway.decorator.CachedRequestDecorator;
import captain.org.gateway.decorator.CachedResponseDecorator;
import captain.org.gateway.entity.RequestParamEntity;
import captain.org.gateway.util.IoUtils;
import captain.org.gateway.util.IpUtils;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class RequestLogFilter implements GlobalFilter, Ordered {
    @Value("${logDB.enabled:false}")
    private boolean dbEnable;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        HttpHeaders headers = exchange.getRequest().getHeaders();
        RequestParamEntity requestParamEntity = convertRequestParam(exchange, headers);
        // 请求装饰
        final CachedRequestDecorator requestDecorator = (CachedRequestDecorator) exchange.getRequest();
        // 响应装饰
        final CachedResponseDecorator responseDecorator = (CachedResponseDecorator) exchange.getResponse();
        return Mono.defer(() -> {
            requestParamEntity.setContentType(headers.getFirst("Content-Type"));
            final boolean jsonReqBody = MediaType.APPLICATION_JSON.isCompatibleWith(headers.getContentType());
            if (jsonReqBody) {
                final String requestData = IoUtils.convertOutputStream(requestDecorator.getCachedBody(), IoUtils.DEFAULT_LIMIT_SIZE);
                requestParamEntity.setRequestData(requestData);
            }
            return Mono.empty();
        }).then(chain.filter(exchange)).then(Mono.create(sink -> {
            final String responseData = IoUtils.convertOutputStream(responseDecorator.getCachedBody(), IoUtils.DEFAULT_LIMIT_SIZE);
            requestParamEntity.setResponseData(responseData);
            if(dbEnable) {
                insertLog(requestParamEntity);
            }
            final long endTime = System.currentTimeMillis();
            requestParamEntity.setEndTime(endTime);
            requestParamEntity.setDuration(endTime - requestParamEntity.getStartTime());
            doPrint(requestParamEntity);
            sink.success();
        }));
    }

    @Override
    public int getOrder() {
        return FilterOrder.LOG.getOrder();
    }

    private Mono<Void> doPrint(RequestParamEntity entity) {
        StringBuffer reqLog = new StringBuffer();
        reqLog.append("\n\n================ Gateway Request Start  ================\n");
        reqLog.append(JSONObject.toJSONString(entity));
        reqLog.append("\n\n================ Gateway Request End  ================\n");
        log.info(reqLog.toString());
        return Mono.empty();
    }

    private Mono<Void> insertLog(RequestParamEntity entity) {
        //TODO  插DB
        log.info("TODO DBDBDBDBDBDBDBDBDBDBDB");
        return Mono.empty();
    }

    private RequestParamEntity convertRequestParam(ServerWebExchange exchange, HttpHeaders headers) {
        final String sourceIp = IpUtils.getIpAddr(exchange);
        final String requestUrl = exchange.getRequest().getURI().getRawPath();
        final String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
        final long startTime = System.currentTimeMillis();
        return RequestParamEntity.builder()
                    .ip(sourceIp)
                    .requestUrl(requestUrl)
                    .token(token)
                    .startTime(startTime)
                    .build();
    }

}
