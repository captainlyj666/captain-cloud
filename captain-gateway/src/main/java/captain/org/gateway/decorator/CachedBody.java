package captain.org.gateway.decorator;

import java.io.ByteArrayOutputStream;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CachedBody {

    /**
     * 使用ByteArrayOutputStream缓存报文.
     *
     * @param body                  报文
     * @param byteArrayOutputStream byteArrayOutputStream
     * @return 无
     */
    default Mono<Void> cacheBody(Flux<DataBuffer> body, ByteArrayOutputStream byteArrayOutputStream) {
        return Mono.create(sink -> DataBufferUtils.write(body, byteArrayOutputStream).subscribe(DataBufferUtils::release, sink::error, sink::success));
    }

    /**
     * 返回缓存报文流.
     *
     * @return 缓存报文流
     */
    ByteArrayOutputStream getCachedBody();

}
