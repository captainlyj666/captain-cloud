package captain.org.gateway.decorator;

import java.io.ByteArrayOutputStream;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * 使用ByteArrayOutputStream缓存的请求.
 *
 * @author Leeyc
 */
public class CachedRequestDecorator extends ServerHttpRequestDecorator implements CachedBody {

    private final ServerWebExchange exchange;
    private final ByteArrayOutputStream byteArrayOutputStream;

    /**
     * 初始化.
     *
     * @param exchange 交换器
     */
    public CachedRequestDecorator(ServerWebExchange exchange) {
        super(exchange.getRequest());
        this.exchange = exchange;
        byteArrayOutputStream = new ByteArrayOutputStream();
    }

    @Override
    public Flux<DataBuffer> getBody() {
        return DataBufferUtils.read(new ByteArrayResource(byteArrayOutputStream.toByteArray()), exchange.getResponse().bufferFactory(), 1024 * 8);
    }

    @Override
    public ByteArrayOutputStream getCachedBody() {
        return byteArrayOutputStream;
    }

    /**
     * 缓存报文.
     *
     * @return 已缓存的报文
     */
    public Mono<Void> cacheBody() {
        return CachedBody.super.cacheBody(exchange.getRequest().getBody(), byteArrayOutputStream);
    }

}
