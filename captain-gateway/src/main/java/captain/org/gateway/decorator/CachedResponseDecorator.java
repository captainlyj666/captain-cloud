package captain.org.gateway.decorator;

import java.io.ByteArrayOutputStream;

import org.reactivestreams.Publisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * 使用ByteArrayOutputStream缓存的响应.
 *
 * @author Leeyc
 */
public class CachedResponseDecorator extends ServerHttpResponseDecorator implements CachedBody {

    private final ServerWebExchange exchange;
    private final ByteArrayOutputStream byteArrayOutputStream;

    /**
     * 初始化.
     *
     * @param exchange        交换器.
     */
    public CachedResponseDecorator(ServerWebExchange exchange) {
        super(exchange.getResponse());
        this.exchange = exchange;
        byteArrayOutputStream = new ByteArrayOutputStream();
    }

    @Override
    public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
        return cacheBody(Flux.from(body), byteArrayOutputStream).then(Mono.defer(() -> {
            byte[] customizedBody = byteArrayOutputStream.toByteArray();
            return super.writeWith(DataBufferUtils.read(new ByteArrayResource(customizedBody), exchange.getResponse().bufferFactory(), 1024 * 8));
        }));
    }

    @Override
    public ByteArrayOutputStream getCachedBody() {
        return byteArrayOutputStream;
    }

}
