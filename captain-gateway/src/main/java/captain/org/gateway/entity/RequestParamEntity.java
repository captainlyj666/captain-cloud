package captain.org.gateway.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestParamEntity {
    private String ip;

    private String requestUrl;

    private String token;

    private String contentType;

    private long startTime;

    private long endTime;

    private long duration;

    private String requestData;

    private String responseData;
}
