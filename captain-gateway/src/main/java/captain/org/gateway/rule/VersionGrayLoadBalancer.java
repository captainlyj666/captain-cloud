package captain.org.gateway.rule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.http.server.reactive.ServerHttpRequest;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 基于客户端版本号灰度路由
 *
 * @author lvyijie
 */
@Slf4j
@AllArgsConstructor
public class VersionGrayLoadBalancer extends GrayLoadBalancer {
	private DiscoveryClient discoveryClient;

    public static final String APP_VERSION = "appVersion";

    public static final String META_VERSION = "version";
	/**
	 * 根据serviceId 筛选可用服务
	 *
	 * @param serviceId 服务ID
	 * @param request   当前请求
	 * @return
	 */
	@Override
	public ServiceInstance choose(String serviceId, ServerHttpRequest request) {
		List<ServiceInstance> instances = discoveryClient.getInstances(serviceId);

		//注册中心无实例 抛出异常
		if (CollectionUtils.isEmpty(instances)) {
			log.warn("No instance available for {}", serviceId);
			throw new NotFoundException("No instance available for " + serviceId);
		}

		// 获取请求version，无则随机返回可用实例
		String reqVersion = request.getHeaders().getFirst(APP_VERSION);
		if (StringUtils.isBlank(reqVersion)) {
			return instances.get(ThreadLocalRandom.current().nextInt(instances.size()));
		}

		// 遍历可以实例元数据，若匹配则返回此实例
		for (ServiceInstance instance : instances) {
			Map<String, String> metadata = instance.getMetadata();
			String targetVersion = Optional.ofNullable(metadata).orElse(new HashMap<>()).get(META_VERSION);
			if (reqVersion.equalsIgnoreCase(targetVersion)) {
				log.debug("gray requst match success :{} {}", reqVersion, instance);
				return instance;
			}
		}
		return instances.get(ThreadLocalRandom.current().nextInt(instances.size()));
	}
}
