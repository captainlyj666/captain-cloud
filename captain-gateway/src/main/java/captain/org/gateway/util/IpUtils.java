package captain.org.gateway.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import org.springframework.web.server.ServerWebExchange;

import com.alibaba.csp.sentinel.util.StringUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Ip工具类.
 *
 */
@Slf4j
public class IpUtils {

    private static final String LOCAL_HOST_IPV4 = "127.0.0.1";
    private static final String LOCAL_HOST_IPV6 = "0:0:0:0:0:0:0:1";
    private static final String SPLIT_STR = ",";
    private static final String UNKNOWN ="unknown";

    /**
     * 获取Ip地址.
     *
     * @param exchange exchange
     * @return ip地址
     */
    public static String getIpAddr(ServerWebExchange exchange) {
        String ip = exchange.getRequest().getHeaders().getFirst("X-Forwarded-For");
        if (StringUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = exchange.getRequest().getHeaders().getFirst("Proxy-Client-IP");
        }
        if (StringUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = exchange.getRequest().getHeaders().getFirst("WL-Proxy-Client-IP");
        }
        if (StringUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = exchange.getRequest().getRemoteAddress().getAddress().getHostAddress();
            if (LOCAL_HOST_IPV4.equalsIgnoreCase(ip) || LOCAL_HOST_IPV6.equalsIgnoreCase(ip)) {
                try {
                    final InetAddress inet = InetAddress.getLocalHost();
                    ip = inet.getHostAddress();
                } catch (UnknownHostException e) {
                    log.error("getIpAddr Exception :", e);
                }
            }
        }
        if (StringUtil.isNotBlank(ip) && ip.indexOf(SPLIT_STR) > 0) {
            ip = ip.substring(0, ip.indexOf(SPLIT_STR));
        }
        return ip;
    }

}
