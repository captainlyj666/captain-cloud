package captain.org.gateway.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IoUtils {

    public static final int DEFAULT_LIMIT_SIZE = 2048;
    public static final String LIMIT_EXCEED_CONTENT = "***";

    /**
     * 输出流转换文字.
     *
     * @param byteArrayOutputStream 输出流
     * @param readLimit             读取限制
     * @return 转换后文字
     */
    public static String convertOutputStream(ByteArrayOutputStream byteArrayOutputStream, int readLimit) {
        if (Objects.isNull(byteArrayOutputStream)) {
            return null;
        }
        try {
            return byteArrayOutputStream.size() > readLimit ? LIMIT_EXCEED_CONTENT : byteArrayOutputStream.toString(StandardCharsets.UTF_8.name()).replaceAll("\\n", "").replaceAll("\\t", "");
        } catch (IOException e) {
            log.error("convertOutputStream Exception :", e);
            return null;
        }
    }

}
