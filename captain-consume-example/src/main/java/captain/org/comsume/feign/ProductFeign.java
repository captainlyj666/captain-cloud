package captain.org.comsume.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "captain-product")
public interface ProductFeign {
    @GetMapping("/app-name/{from}")
    String echoAppName(@PathVariable("from") String from);
}
