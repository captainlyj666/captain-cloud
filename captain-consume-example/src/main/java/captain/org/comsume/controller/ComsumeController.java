package captain.org.comsume.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import captain.org.comsume.feign.ProductFeign;

@RestController
public class ComsumeController {
    @Autowired
    ProductFeign productFeign;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${server.port}")
    private String serverPort;

    @Value("${custom.test-value:none}")
    private String testValue;

    @GetMapping("/test")
    public String getTestValue() {
        return testValue + "@" + serverPort;
    }

    @GetMapping("/testFegin")
    public String testFegin() {
        return productFeign.echoAppName("123");
    }
}
